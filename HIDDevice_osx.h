#pragma once

#include "HIDSensor.h"

class HIDDevice
{
    IOHIDManagerRef m_hidManager;
    IOHIDDeviceRef  m_device;
    unsigned char   m_buffer[96];
    char            m_productName[64];
    char            m_vendorName[64];
    char            m_serialNumber[64];
    int             m_vendor;
    int             m_product;
    int             m_version;
    std::thread     m_readThread;
    std::mutex      m_queueMutex;
    std::condition_variable m_queueCond;
    std::queue<HIDSensorMessage> m_queue;
    bool            m_active;

    static void reportInput_callback(void* context, IOReturn result, void* sender, IOHIDReportType type,
        uint32_t reportID, uint8_t* report, CFIndex reportLength);

    IOHIDDeviceRef findRiftDevice();
    void           readDeviceStrings();
    void           readAttributes();
    void           onData(unsigned char* data, long sz);
    void           readLoop();

public:
    HIDDevice() : m_device(NULL){}
    ~HIDDevice() { close(); }

    bool init();
    unsigned long read(HIDMessage& sens);
    void close();
    void getFeature(HIDMessage& feat) const;
    void setFeature(HIDMessage& feat) const;
    void setFeature(const HIDMessage& feat) const;
    const char* getProductName() const { return m_productName; }
    const char* getVendorName() const { return m_vendorName; }
    const char* getSerialString() const { return m_serialNumber; }
    int getVendorID() const { return m_vendor; }
    int getProductID() const { return m_product; }
    int getVersionNumber() const { return m_version; }
};
