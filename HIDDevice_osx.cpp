#include "HIDDevice_osx.h"

void HIDDevice::reportInput_callback(void* context, IOReturn result, void* sender, IOHIDReportType type,
    uint32_t reportID, uint8_t* report, CFIndex reportLength)
{
    HIDDevice* dev = (HIDDevice*)context;
    dev->onData(report, reportLength);
}

void HIDDevice::onData(unsigned char *data, long sz)
{
    if (!m_active) return;
    std::unique_lock<std::mutex> lock(m_queueMutex);
    m_queue.emplace(data, sz);
    m_queueCond.notify_all();
}

void HIDDevice::readLoop()
{
    IOHIDDeviceScheduleWithRunLoop(m_device, CFRunLoopGetCurrent(), kCFRunLoopDefaultMode);
    IOHIDDeviceRegisterInputReportCallback(m_device, m_buffer, sizeof(m_buffer), reportInput_callback, this);
    m_active = true;
    int res = 0;
    while((res != kCFRunLoopRunFinished) && m_active/* && res != kCFRunLoopRunTimedOut*/)
    {
        res = CFRunLoopRunInMode(kCFRunLoopDefaultMode, 0.01, FALSE);
	}
}

bool HIDDevice::init()
{
    if (m_active) return 0;
    
    m_hidManager = IOHIDManagerCreate(kCFAllocatorDefault, kIOHIDOptionsTypeNone);
    if(!m_hidManager)
        return false;
    
    m_active = true;
    
    IOHIDManagerSetDeviceMatching(m_hidManager, NULL);
    if(IOHIDManagerOpen(m_hidManager, kIOHIDOptionsTypeNone) != kIOReturnSuccess)
    {
        close();
        return false;
    }
    
    m_device = findRiftDevice();
    if (!m_device)
        return false;
    
    readDeviceStrings();
    readAttributes();
    
    m_readThread = std::thread(&HIDDevice::readLoop, this);
    
    return true;
}

void HIDDevice::close()
{
    if (!m_active) return;
    
    m_active = false;
    
    if(m_readThread.joinable())
        m_readThread.join();
    
    if (m_device)
    {
        IOHIDDeviceClose(m_device, kIOHIDOptionsTypeSeizeDevice);
        CFRelease(m_device);
    }
    if(m_hidManager)
    {
        IOHIDManagerClose(m_hidManager, kIOHIDOptionsTypeNone);
        CFRelease(m_hidManager);
    }
    m_device = NULL;
    m_hidManager = NULL;
    
    // unblock threads
    m_queueCond.notify_all();
}

void HIDDevice::setFeature(HIDMessage& feat) const
{
    if (!m_active) return;

    feat.pack();
    unsigned char* buffer = ((HIDMessage*)&feat)->getBuffer();
    CFIndex reportID = buffer[0];
    IOHIDDeviceSetReport(m_device, kIOHIDReportTypeFeature, reportID, buffer, feat.getBufferSize());
}

void HIDDevice::setFeature(const HIDMessage& feat) const
{
    if (!m_active) return;
    
    unsigned char* buffer = ((HIDMessage*)&feat)->getBuffer();
    CFIndex reportID = buffer[0];
    IOHIDDeviceSetReport(m_device, kIOHIDReportTypeFeature, reportID, buffer, feat.getBufferSize());
}

void HIDDevice::getFeature(HIDMessage& feat) const
{
    if (!m_active) return;

    unsigned char* buffer = feat.getBuffer();
    CFIndex reportID = buffer[0];
    CFIndex reportLength = feat.getBufferSize();
    IOHIDDeviceGetReport(m_device, kIOHIDReportTypeFeature, reportID, buffer, &reportLength);
    feat.unpack();
}

void HIDDevice::readDeviceStrings()
{
    CFStringRef str;
    CFIndex len;
    
    if (!m_active) return;

    std::memset(m_productName, 0, sizeof(m_productName));
    std::memset(m_vendorName, 0, sizeof(m_vendorName));
    std::memset(m_serialNumber, 0, sizeof(m_serialNumber));

    str = (CFStringRef)IOHIDDeviceGetProperty(m_device, CFSTR(kIOHIDProductKey));
    if (str)
    {
        len = CFStringGetLength(str);
        CFStringGetBytes(str, CFRangeMake(0, len), kCFStringEncodingUTF8,
            '?', FALSE, (UInt8*)m_productName, sizeof(m_productName), NULL);
    }
    
    str = (CFStringRef)IOHIDDeviceGetProperty(m_device, CFSTR(kIOHIDManufacturerKey));
    if (str)
    {
        len = CFStringGetLength(str);
        CFStringGetBytes(str, CFRangeMake(0, len), kCFStringEncodingUTF8,
            '?', FALSE, (UInt8*)m_vendorName, sizeof(m_vendorName), NULL);
    }
    
    str = (CFStringRef)IOHIDDeviceGetProperty(m_device, CFSTR(kIOHIDSerialNumberKey));
    if (str)
    {
        len = CFStringGetLength(str);
        CFStringGetBytes(str, CFRangeMake(0, len), kCFStringEncodingUTF8,
            '?', FALSE, (UInt8*)m_serialNumber, sizeof(m_serialNumber), NULL);
    }
}

void HIDDevice::readAttributes()
{
    CFNumberRef res;
    
    if (!m_active) return;
    
    res = (CFNumberRef)IOHIDDeviceGetProperty(m_device, CFSTR(kIOHIDVendorIDKey));
    if(res) CFNumberGetValue(res, kCFNumberSInt32Type, &m_vendor);
    res = (CFNumberRef)IOHIDDeviceGetProperty(m_device, CFSTR(kIOHIDProductIDKey));
    if(res) CFNumberGetValue(res, kCFNumberSInt32Type, &m_product);
    res = (CFNumberRef)IOHIDDeviceGetProperty(m_device, CFSTR(kIOHIDVersionNumberKey));
    if(res) CFNumberGetValue(res, kCFNumberSInt32Type, &m_version);
}

IOHIDDeviceRef HIDDevice::findRiftDevice()
{
    long count;
    IOHIDDeviceRef* devices, rift = NULL;
    
    CFSetRef devicesSet = IOHIDManagerCopyDevices(m_hidManager);
    count = CFSetGetCount(devicesSet);
    devices = new IOHIDDeviceRef[count];
    CFSetGetValues(devicesSet, (const void**)devices);
    
    for (int i = 0; i < count; i++)
    {
        unsigned int vendorID, productID;
        CFTypeRef res;
        res = IOHIDDeviceGetProperty(devices[i], CFSTR(kIOHIDVendorIDKey));
        if(!res) continue;
        CFNumberGetValue((CFNumberRef)res, kCFNumberSInt32Type, &vendorID);
        res = IOHIDDeviceGetProperty(devices[i], CFSTR(kIOHIDProductIDKey));
        if(!res) continue;
        CFNumberGetValue((CFNumberRef)res, kCFNumberSInt32Type, &productID);
        if(vendorID == 0x2833 && productID == 0x0001)
        {
            if(IOHIDDeviceOpen(devices[i], kIOHIDOptionsTypeSeizeDevice) == kIOReturnSuccess)
            {
                rift = devices[i];
                CFRetain(rift);
                break;
            }
        }
    }
    free(devices);
    CFRelease(devicesSet);
    return rift;
}

unsigned long HIDDevice::read(HIDMessage& sens)
{
    if (!m_active) return 0;
    std::unique_lock<std::mutex> lock(m_queueMutex);
    while(m_queue.empty() && m_active)
        m_queueCond.wait(lock);
    
    // just return if unlocked to terminate
    if (!m_active) return 0;
    
    sens = m_queue.front();
    m_queue.pop();
    return 1;
}
