#pragma once

class HIDMessage
{
protected:
    bool isBigEndian() const
    {
        union { int n; char c[4]; } test = { 0x01000000 };
        return test.c[0] == 1;
    }
    inline unsigned char unpackU8(const unsigned char* buffer) const
    {
        return buffer[0];
    }
    inline unsigned short unpackU16(const unsigned char* buffer) const
    {
        return buffer[0] | static_cast<unsigned short>(buffer[1]) << 8;
    }
    inline short unpackS16(const unsigned char* buffer) const
    {
        return buffer[0] | static_cast<short>(buffer[1]) << 8;
    }
    inline unsigned long unpackU32(const unsigned char* buffer) const
    {
        return buffer[0]
            | static_cast<unsigned long>(buffer[1]) << 8
            | static_cast<unsigned long>(buffer[2]) << 16
            | static_cast<unsigned long>(buffer[3]) << 24;
    }
    inline float unpackF32(const unsigned char* buffer) const
    {
        return *reinterpret_cast<float*>(&buffer);
    }
    inline void packU8(unsigned char value, unsigned char* buffer) const
    {
        buffer[0] = value;
    }
    inline void packU16(unsigned short value, unsigned char* buffer) const
    {
        buffer[0] = static_cast<unsigned char>(value);
        buffer[1] = static_cast<unsigned char>(value >> 8);
    }
    inline void packS16(short value, unsigned char* buffer) const
    {
        buffer[0] = static_cast<unsigned char>(value);
        buffer[1] = static_cast<unsigned char>(value >> 8);
    }
    inline void packU32(unsigned long value, unsigned char* buffer) const
    {
        buffer[0] = static_cast<unsigned char>(value);
        buffer[1] = static_cast<unsigned char>(value >> 8);
        buffer[2] = static_cast<unsigned char>(value >> 16);
        buffer[3] = static_cast<unsigned char>(value >> 24);
    }
    inline void packF32(float value, unsigned char* buffer) const
    {
        *reinterpret_cast<float*>(&buffer) = value;
    }

public:
    virtual void pack() = 0;
    virtual void unpack() = 0;
    virtual unsigned char* getBuffer() = 0;
    virtual int getBufferSize() const = 0;
    virtual HIDMessage& operator=(const HIDMessage& m) = 0;
};
