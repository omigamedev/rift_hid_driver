#include "pch.h"
#include "HIDDevice.h"
#include "HIDFeatures.h"
#include "HIDSensor.h"
#include "cbuffer.h"
#include "draw.h"
#include "MadgwickAHRS.h"

static int g_width = 500;
static int g_heigth = 500;
static volatile bool active = true;
static GLFWwindow* window;
static const char* title = "Rift Sensor Fusion";

cbuffer<glm::vec3> accs(100);
cbuffer<glm::vec3> gyrs(100);
cbuffer<glm::vec3> mags(100);
cbuffer<glm::vec3> waccs(100);
cbuffer<glm::vec3> saccs(10);
cbuffer<glm::vec3> wmags(100);
std::vector<std::pair<glm::vec3, glm::vec3>> points; // [position, mag-value]
glm::vec3 lastPoint;
glm::vec3 wacc;
glm::vec3 pos;
glm::quat q;
glm::vec2 cameraAngle;
float cameraZ = -2;

bool drag = false;
glm::vec2 mousePos;
glm::ivec2 mouseStart;

void render()
{
//    float ahrs_q[] = {q0, q1, q2, q3};
//    q = glm::make_quat(ahrs_q);
    
    // Orthogonal matrix
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    // Clear the framebuffer
    glViewport(0, 0, g_width, g_heigth);
    glClearColor(1.f, 1.f, 1.f, 1.f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Draw acceleration in bottom-left
    glViewport(0, 0, g_width / 2, g_heigth / 2);
    drawSignal(wmags, &glm::vec3::x, glm::vec3(1, 0, 0));
    drawSignal(wmags, &glm::vec3::y, glm::vec3(0, 1, 0));
    drawSignal(wmags, &glm::vec3::z, glm::vec3(0, 0, 1));

    // Draw gyroscope in bottom-right
    glViewport(g_width / 2, 0, g_width / 2, g_heigth / 2);
    drawSignal(accs, &glm::vec3::x, glm::vec3(1, 0, 0));
    drawSignal(accs, &glm::vec3::y, glm::vec3(0, 1, 0));
    drawSignal(accs, &glm::vec3::z, glm::vec3(0, 0, 1));

    // Draw magnetometer in top-left
    glViewport(0, g_heigth / 2, g_width / 2, g_heigth / 2);
    drawSignal(mags, &glm::vec3::x, glm::vec3(1, 0, 0));
    drawSignal(mags, &glm::vec3::y, glm::vec3(0, 1, 0));
    drawSignal(mags, &glm::vec3::z, glm::vec3(0, 0, 1));

    // Go to perspective mode
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glm::mat4 proj = glm::perspective(45.f, (float)g_width / (float)g_heigth, .1f, 1000.f);
    glMultMatrixf(glm::value_ptr(proj));
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    // Enable scissors and clear the background
    glEnable(GL_SCISSOR_TEST);
    glViewport(g_width / 2, g_heigth / 2, g_width / 2, g_heigth / 2);
    glScissor(g_width / 2, g_heigth / 2, g_width / 2, g_heigth / 2);
    glClearColor(.3f, .3f, .3f, 1.f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glLoadIdentity();
    glm::mat4 camera = glm::translate(glm::vec3(0, 0, cameraZ)) * 
        glm::eulerAngleXY(cameraAngle.x, cameraAngle.y);
    glMultMatrixf(glm::value_ptr(camera));

    glm::vec3 red(1, 0, 0);
    glm::vec3 green(0, 1, 0);
    glm::vec3 blue(0, 0, 1);
    glm::vec3 white(1, 1, 1);

    // Draw axis
    drawVector(glm::vec3(1, 0, 0) * 2.f, red * .3f);
    drawVector(glm::vec3(0, 1, 0) * 2.f, green * .3f);
    drawVector(glm::vec3(0, 0, 1) * 2.f, blue * .3f);

    // Draw current orientation
    drawVector(q * glm::vec3(1, 0, 0), red);
    drawVector(q * glm::vec3(0, 1, 0), green);
    drawVector(q * glm::vec3(0, 0, 1), blue);

    // Draw current orientation
//    glm::vec3 mag = q * glm::normalize(mags.average());
//    glm::vec3 mag2(sqrtf(mag.x*mag.x+mag.y*mag.y), 0, mag.z);
//    drawVector(glm::vec3(mag2.x, 0, 0), red);
//    drawVector(glm::vec3(0, mag2.y, 0), green);
//    drawVector(glm::vec3(0, 0, mag2.z), blue);

    drawVector(glm::normalize(q * glm::normalize(mags.average())), white);
    drawVector(glm::normalize(glm::normalize(mags.average())), red);

    // draw points cloud
    glPointSize(2);
    glBegin(GL_POINTS);
    for (auto p : points)
    {
        glColor3f(p.second.r, p.second.g, p.second.b);
        glVertex3f(p.first.x, p.first.y, p.first.z);
    }
    glEnd();
    glBegin(GL_LINES);
    for (auto p : points)
    {
        glm::vec3 p2 = p.first + p.second * 0.05f;
        glColor3f(p.second.r, p.second.g, p.second.b);
        glVertex3f(p.first.x, p.first.y, p.first.z);
        glVertex3f(p2.x, p2.y, p2.z);
    }
    glEnd();

    glm::vec3 mavg = wmags.average();
    glm::vec3 ppos = q * glm::vec3(0, 0, -1);
    float dist = glm::length(ppos - lastPoint);
    //mavg.y = 0;
    if (dist > 0.1f)
    {
        points.emplace_back(ppos, glm::normalize(mavg) * 0.5f + 0.5f);
        lastPoint = ppos;
    }

    glm::vec3 t = pos * 0.1f;
    glTranslatef(t.x, t.y, t.z);

    // Orientation by gyroscope
    glMultMatrixf(glm::value_ptr(glm::mat4_cast(q)));

    glScalef(.07f, .07f, .07f);
    drawCube();

    glDisable(GL_SCISSOR_TEST);
}

int main()
{
    HIDDevice rift;
    HIDFeatureDisplayInfo display;

    // Find and initialize Rift device
    if(!rift.init())
    {
        MSGBOX("Error", "Rift device not found.");
        return -1;
    }

    // Activate the device to receive commands
    rift.setFeature(HIDFeatureKeepAlive(1000));

    // Read Rift's display information
    rift.getFeature(display);

    // Read default sensors range
    HIDFeatureSensorRange sr;
    rift.setFeature(HIDFeatureSensorRange(
        HIDFeatureSensorRange::AccelerationRange::Default,
        HIDFeatureSensorRange::RotationRange::Default,
        HIDFeatureSensorRange::MagneticRange::Gauss_0_88));

    std::printf("Sensor range\n"
        "- Acc: %d\n"
        "- Gyr: %d\n"
        "- Mag: %d\n",
        sr.acc, sr.gyr, sr.mag);
    
    // Print some device info
    std::printf("Device info\n"
        "- Name: %s\n"
        "- Vendor: %s\n"
        "- Serial: %s\n"
        "- Resolution: %dx%d\n",
        rift.getProductName(),
        rift.getVendorName(),
        rift.getSerialString(),
        display.resolution[0],
        display.resolution[1]);
    
    // Init the buffer to avoid rendering nothing
    accs.add(glm::vec3());
    gyrs.add(glm::vec3());
    mags.add(glm::vec3());

    // Create a thread that keeps alive the sensors
    std::thread keepAliveThread([&rift](){
        while (active)
        {
            rift.setFeature(HIDFeatureKeepAlive(10000));
            std::this_thread::sleep_for(std::chrono::milliseconds(9000));
        }
    });

    // Create a thread that reads the sensors
    std::thread readSensors([&rift](){
        // Read and print the sensors value
        HIDSensorMessage m;
        int samples = 0;
        auto start = std::chrono::system_clock::now();
        glm::vec3 oldgyr;
        while (active && rift.read(m))
        {
            auto end = std::chrono::system_clock::now();
            std::chrono::duration<double> diff = end-start;
            start = end;
            m.unpack();
            accs.add(m.acc[0]);
            gyrs.add(m.gyr[0]);
            mags.add(m.mag);
            waccs.add(q * m.acc[0]);
            saccs.add(m.acc[0]);
            wmags.add(q * m.mag);

            float dt = static_cast<float>(diff.count());

            // Quaterion method
            float velLen = glm::length(m.gyr[0]);
            q = glm::normalize(q * glm::angleAxis(glm::degrees(velLen * dt), m.gyr[0] / velLen));
//            float quat_raw[] = {m.gyr[0].x, m.gyr[0].y, m.gyr[0].z, 0.f};
//            glm::quat derivative = (0.5f * q) * glm::make_quat(quat_raw);
//            q = glm::normalize(q + derivative * dt);

            // Correct rotation
            glm::vec3 wacc = glm::normalize(q * accs.average());
            glm::vec3 tiltAxis = glm::cross(glm::vec3(0,1,0), wacc);
            float crossLen = glm::length(tiltAxis);
            //if (fabsf(crossLen) > 0.1)
            {
                float tiltAngle = glm::asin(crossLen);
                q = glm::angleAxis(-tiltAngle * 0.2f, tiltAxis / crossLen) * q;
            }
            q = glm::normalize(q);

            
            samples++;
            oldgyr = m.gyr[0];
        }
    });

    //** INIT GLFW **

    if (!glfwInit())
        return -1;

    int monitors_count;
    GLFWmonitor** monitors = glfwGetMonitors(&monitors_count);
    const GLFWvidmode* mode = glfwGetVideoMode(monitors[monitors_count - 1]);
    g_width = mode->width;
    g_heigth = mode->height;

    if (!(window = glfwCreateWindow(g_width, g_heigth, title, NULL, NULL)))
    {
        glfwTerminate();
        MSGBOX("Error", "Cannot create main window.");
        return -1;
    }

    int x, y;
    glfwGetMonitorPos(monitors[monitors_count - 1], &x, &y);
    glfwSetWindowPos(window, x, y);

    glfwSetWindowSizeCallback(window, [](GLFWwindow* window, int w, int h)
    {
        g_width = w;
        g_heigth = h;
    });
    
    glfwSetKeyCallback(window, (GLFWkeyfun)[](GLFWwindow* window,
        int key, int scancode, int action, int mods)
    {
        if (action == GLFW_PRESS)
        {
            switch (key)
            {
                case GLFW_KEY_ESCAPE:
                    active = false;
                    break;
                case GLFW_KEY_R:
                    q = glm::quat();
                    points.clear();
                default:
                    break;
            }
        }
    });

    glfwSetScrollCallback(window, (GLFWscrollfun)[](GLFWwindow*, double x, double y)
    {
        cameraZ += (float)(y * 0.1);
    });

    glfwSetCursorPosCallback(window, (GLFWcursorposfun)[](GLFWwindow*, double x, double y)
    {
        static double oldx = x;
        static double oldy = y;
        mousePos = glm::vec2(x, y);
        if (drag)
        {
            cameraAngle.y += -(float)((x - oldx) * 0.01);
            cameraAngle.x += (float)((y - oldy) * 0.01);
        }
        oldx = x;
        oldy = y;
    });

    glfwSetMouseButtonCallback(window, (GLFWmousebuttonfun)[](GLFWwindow*, int button, int action, int mod)
    {
        if (button == GLFW_MOUSE_BUTTON_LEFT)
        {
            drag = (action == GLFW_PRESS);
            mouseStart = mousePos;
        }
    });
    
    glfwMakeContextCurrent(window);

    glEnable(GL_DEPTH_TEST);

    // Main loop
    while (!glfwWindowShouldClose(window) && active)
    {
        render();
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    active = false;
    rift.close();
    glfwTerminate();
    keepAliveThread.detach();
    readSensors.join();

    return 0;
}
