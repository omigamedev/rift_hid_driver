#pragma once

#ifdef _WIN32
#include "pch.h"
#endif

#include "HIDMessage.h"

class HIDSensorMessage : public HIDMessage
{
    unsigned char buffer[62];
public:
    unsigned char sampleCount;
    unsigned short timestamp;
    unsigned short lastCommand;
    float temperature;
    glm::vec3 acc[3], gyr[3], mag;

    HIDSensorMessage() {}
    HIDSensorMessage(const unsigned char* data, long size)
    {
        std::memcpy(buffer, data, size);
        unpack();
    }
    
    HIDMessage& operator=(const HIDMessage& m) override
    {
        const HIDSensorMessage& sm = dynamic_cast<const HIDSensorMessage&>(m);
        std::memcpy(buffer, sm.buffer, sizeof(buffer));
        sampleCount = sm.sampleCount;
        timestamp = sm.timestamp;
        lastCommand = sm.lastCommand;
        temperature = sm.temperature;
        for (int i = 0; i < 3; i++)
        {
            acc[i] = sm.acc[i];
            gyr[i] = sm.gyr[i];
        }
        mag = sm.mag;
        return *this;
    }
    
    void unpackVector(const unsigned char* buffer, glm::vec3& v)
    {
        // Sign extending trick
        // from http://graphics.stanford.edu/~seander/bithacks.html#FixedSignExtend
        struct { int x : 21; } s;

        v.x = (s.x = (buffer[0] << 13) | (buffer[1] << 5) | ((buffer[2] & 0xF8) >> 3)) * 0.0001f;
        v.y = (s.x = ((buffer[2] & 0x07) << 18) | (buffer[3] << 10) | (buffer[4] << 2) | ((buffer[5] & 0xC0) >> 6)) * 0.0001f;
        v.z = (s.x = ((buffer[5] & 0x3F) << 15) | (buffer[6] << 7) | (buffer[7] >> 1)) * 0.0001f;
    }

    virtual void pack() override
    {
        buffer[0] = 0;
    }
    virtual void unpack() override
    {
        sampleCount = buffer[0];
        timestamp = unpackU16(buffer + 1);
        lastCommand = unpackU16(buffer + 4);
        temperature = unpackS16(buffer + 6) * 0.01f;

        int n = std::min<int>(3, sampleCount);
        for (int i = 0; i < n; i++)
        {
            unpackVector(buffer + 8 + 16 * i, acc[i]);
            unpackVector(buffer + 16 + 16 * i, gyr[i]);
        }

        mag.x = unpackS16(buffer + 56) * 0.001f;
        mag.y = unpackS16(buffer + 58) * 0.001f;
        mag.z = unpackS16(buffer + 60) * 0.001f;
    }
    virtual unsigned char* getBuffer() override
    {
        return buffer;
    }
    virtual int getBufferSize() const override
    {
        return sizeof(buffer);
    }
};
